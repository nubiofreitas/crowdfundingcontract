# Install dependencies
```
npm install

```

### compile the contracts
> cd .\ethereum\

## Compile contract
```
node compile.js

```
## tests
```
npm run test 

```
## Deploy the contract
```
node deploy.js

```
## Run and fun
```
npm run dev

```
