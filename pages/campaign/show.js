import React, { Component } from 'react';
import { Grid, Card, GridColumn, Button  } from 'semantic-ui-react';
import Campaign from '../../ethereum/campaign';
import web3 from '../../ethereum/web3';
import { Link } from '../../routes';
import FormContribute from '../../components/contributeForm';

class Show extends Component {
    
    static async getInitialProps(props) {

        const campaign = Campaign(props.query.address);

        const summary = await campaign.methods.getSummary().call();
        //console.log(summary);
        return {
            minimumContribution:  summary[0],
            balance: summary[1],
            requests: summary[2],
            contributors: summary[3],
            manager: summary[4],
            address: props.query.address
        }
    }

    renderCards = () => {
        const { minimumContribution, 
            balance, 
            requests, 
            contributors, 
            manager,
            address } = this.props;

        const items = [
            {
                header: manager,
                description: 'Este endereço é do criador da campanha e quem poderá fazer requisições de retirada',
                meta: 'Endereço do manager',
                style: { overflowWrap : 'break-word' }
            },{
                header: web3.utils.fromWei(minimumContribution, 'ether'),
                description: 'Contribuição Mínima (Ether)',
                meta: 'Contribuição mínima (Ether)'               
            },{
                    header: web3.utils.fromWei(balance, 'ether'),
                    description: 'Aqui fica todo valor arrecadado por esta campanha',
                    meta: 'Balanço da campanha (Ether)'
                },
            {
                header: requests,
                description: 'Requisições de retirada',
                meta: 'Requisições',
                extra: <Link route={`/campaign/${address}/request`}><a>Ver Requisições</a></Link>
            },{
                header: contributors,
                description: 'Total de contribuidores',
                meta: 'Contribuidores'
            }
        ];
    
        return <Card.Group floated="left" items={items} />
    }

    render(){
        const { address, minimumContribution }  = this.props;
        
        return(
            <Grid>
                <Grid.Row>
                    <Grid.Column width={10}>
                        {this.renderCards()}
                    </Grid.Column>
                    <Grid.Column floated="right" width={6}>
                        <FormContribute address={address} minimumContribution={minimumContribution} onReloadCards={this.renderCards()}/>
                    </Grid.Column>  
                </Grid.Row>
                <Grid.Row>
                    <GridColumn>
                        <Link route={`/campaign/${address}/request`}>
                            <a>
                                <Button
                                        //não vou usar onclick para dar a possibilidade de clicar com o direito e abrir em nova aba
                                        //onClick={this.onClick}
                                        floated="left" 
                                        content="View Requests"
                                        icon="eye"
                                        primary
                                        />
                            </a>
                        </Link>
                    </GridColumn>
                </Grid.Row>
            </Grid>
        )
    }
}

export default Show;