import React, { Component } from 'react';
import { Button, Grid, Table } from 'semantic-ui-react';
import Campaign from '../../../ethereum/campaign';
import { Link } from '../../../routes';
import RequestRow from '../../../components/requestRow';

class Show extends Component {

    static async getInitialProps(props){
        const campaign = Campaign(props.query.address);
        const requestCount = await campaign.methods.getRequestCount().call();
        const approversCount = await campaign.methods.contributors().call();

        console.log("request count: " + requestCount);

        const requests = await Promise.all(
            Array(parseInt(requestCount)).fill().map((element, index) => {
                return campaign.methods.request(index).call();
            })
        );       

        return {
            address: props.query.address,
            requests: requests,
            requestCount: requestCount,
            approversCount: approversCount
        };
    }

    renderRow(){
        return this.props.requests.map((request, index) => {
            return <RequestRow 
                key={index}
                id={index}
                request={request}
                address={this.props.address}
                aproversCount={this.props.approversCount}
            />;
        });
    }

    render() {
        const { address } = this.props;
        const { Header, Row, HeaderCell, Body } = Table;

        return (            
                <div>
                    <h3>Requisições</h3>
                    <Grid>
                        <Grid.Row>
                            <Grid.Column floated="right" width={3}>
                                <Link route={`/campaign/${address}/request/new`}>
                                    <a>
                                        <Button
                                            floated="left" 
                                            content="New Request"
                                            icon="plus circle"
                                            primary
                                        />
                                    </a>
                                </Link>
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                            <Grid.Column>
                                <Table>
                                    <Header>
                                        <Row>
                                            <HeaderCell>ID</HeaderCell>
                                            <HeaderCell>Description</HeaderCell>
                                            <HeaderCell>Amount(Eth)</HeaderCell>
                                            <HeaderCell>Recipient</HeaderCell>
                                            <HeaderCell>Approval Count</HeaderCell>
                                            <HeaderCell>Approve</HeaderCell>
                                            <HeaderCell>Finalize</HeaderCell>
                                        </Row>
                                    </Header>
                                    <Body>
                                        {this.renderRow()}
                                    </Body>                               
                                </Table>
                                <div>{this.props.requestCount > 1 ? 'Encontradas' : 'Encontrada'} {this.props.requestCount} {this.props.requestCount > 1 ? 'Requisições' : 'Requisição'}</div>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </div>
        );
    }
}

export default Show;