import React, { Component } from 'react';
import { Form, Button, Grid, Input, Message } from 'semantic-ui-react';
import Campaign from '../../../ethereum/campaign';
import web3 from '../../../ethereum/web3';
import { Router } from '../../../routes';

class NewRequest extends Component {
    
    
    static async getInitialProps(props) {

        const campaign = Campaign(props.query.address);
        
        return {
            address: props.query.address
        };
    }

    state = {
        description: '',
        value: '',
        errorMessage: '',       
        loading: false,
        recipient: ''
    }

    handleChange = (e, {name, value}) => this.setState({ [name]: value });

    onSubmit = async (event) => {
        event.preventDefault();
        
        const { description, value, recipient } = this.state;
        const { address } = this.props;
        
        try{
            this.setState({ loading: true, errorMessage: '' });
            const campaign = Campaign(address);
            const accounts = await web3.eth.getAccounts();                    
            await campaign.methods.createRequest(description, web3.utils.toWei(value, 'ether'), recipient).send({ from: accounts[0] });
            
            Router.pushRoute(`/campaign/${address}/request`);
        }catch(err){
            this.setState({ errorMessage: err.message });
        }finally{
            this.setState({ loading: false });
        }
    }
    
    render() {
        
        const { description, 
                value, 
                recipient, 
                loading, 
                errorMessage
                 } = this.state;

        return (            
                <div>
                    <h3>Nova Requisição</h3>
                    <Form onSubmit={this.onSubmit} error={errorMessage}>
                        <Grid>
                            <Grid.Row>
                                <Grid.Column width={6}>
                                    <Form.Field>
                                        <label>Description</label>
                                        <Input
                                            autoComplete='off'
                                            icon='file alternate'                                    
                                            //actionPosition='left'
                                            //label="wei" 
                                            //labelPosition="right"
                                            placeholder="Description of the request..."
                                            name="description"
                                            value={description}
                                            onChange={this.handleChange}
                                        />
                                    </Form.Field>
                                    <Form.Field>
                                        <label>Amount</label>
                                        <Input
                                            autoComplete='off'
                                            icon='ethereum'                                    
                                            //actionPosition='left'
                                            //label="wei" 
                                            //labelPosition="right"
                                            placeholder="Amount of ether..."
                                            name="value"
                                            value={value}
                                            onChange={this.handleChange}
                                        />
                                    </Form.Field>
                                    <Form.Field>
                                        <label>Endereço de destino</label>
                                        <Input
                                            autoComplete='off'
                                            icon='arrow right'                                    
                                            //actionPosition='left'
                                            //label="wei" 
                                            //labelPosition="right"
                                            placeholder="Address..."
                                            name="recipient"
                                            value={recipient}
                                            onChange={this.handleChange}
                                        />
                                    </Form.Field>
                                    <Button loading={loading} disabled={loading} primary icon="add circle" content="Criar" />
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                        <Message error header="Oops!" content={errorMessage}/>                       
                    </Form>
                </div>            
        );
    }
}

export default NewRequest;