import React, { Component } from 'react';
import { Form, Button, Grid, Input, Message } from 'semantic-ui-react';
import factory from '../../ethereum/factory';
import web3 from '../../ethereum/web3';
import { Router } from '../../routes';

class CampaignNew extends Component  {
    
    state = {
        amount: '',
        errorMessage: '',
        loading: false
    }

    handleChange = (e, {name, value}) => this.setState({ [name]: value });

    onSubmit = async (e) => {
        e.preventDefault();
        const { amount } = this.state;
        try{
            this.setState({ loading: true, errorMessage: '' });
            //code here
            const accounts = await web3.eth.getAccounts();
            
            await factory.methods.createCampaign(web3.utils.toWei(amount, 'ether')).send({ from: accounts[0] });

            Router.pushRoute('/'); 
        }catch(err){
            console.info('erroMetmask', err.message)
            this.setState({ errorMessage: err.message })
        }finally{
            this.setState({ loading: false });

        }
    }

    render(){
        const { amount } = this.state;

        return(
                <Form onSubmit={this.onSubmit} error={this.state.errorMessage}>
                    <Grid>
                        <Grid.Column width={4}>
                            <Form.Field>
                                <label>Definir Contribuição Mínima Ether</label>
                                <Input                                  
                                    autoComplete='off'
                                    icon='ethereum'                                    
                                    //actionPosition='left'
                                    //label="wei" 
                                    //labelPosition="right"
                                    placeholder="Contribuição Mínima"
                                    name="amount"
                                    value={ amount }
                                    onChange={this.handleChange}
                                />
                            </Form.Field>
                            <Button 
                                loading={this.state.loading} 
                                disabled={this.state.loading} 
                                primary 
                                icon="add circle" 
                                content="Criar" 
                            />
                        </Grid.Column>

                    </Grid>
                    <Message error header="Oops!" content={this.state.errorMessage}/>
                </Form>            
        )
    }
}

export default CampaignNew;