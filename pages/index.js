import React, { Component } from 'react';
import { Button, Card } from 'semantic-ui-react';
import { Link } from './../routes';
import factory from '../ethereum/factory';

class CampaignIndex extends Component {
    static async getInitialProps() {
        const campaigns = await factory.methods.getDeployedCampaigns().call();
        return { campaigns };        
    }

    renderCampaings = () => {
        const items = this.props.campaigns.map(address => {
            return {
                header: address,
            description: (
                <Link route={`/campaign/${address}`}>
                    <a>Ver Campanha</a>
                </Link>
                ),
                fluid: true
            };
        });
       
        return <Card.Group items={items}/>
    }

    render(){
        return(            
                <div>
                    <h3>Campanhas abertas</h3>
                    <Link route="/campaign/new">
                        <a>
                            <Button
                                //não vou usar onclick para dar a possibilidade de clicar com o direito e abrir em nova aba
                                //onClick={this.onClick}
                                floated="right" 
                                content="Criar Campanha"
                                icon="add circle"
                                primary
                                />
                        </a>
                    </Link>
                    {this.renderCampaings()}
                </div>           
        )
    }
}

export default CampaignIndex;