const mnemonic = require('./mnemonicProvider').mnemonic;

const urlProvider = require('./mnemonicProvider').urlProvider;

const HDWalletProvider = require('truffle-hdwallet-provider');

const fs = require('fs-extra');


const Web3 = require('web3');

const compiledFactory = require('./build/CrowdfundingFactory.json');

/**
 * @param mnemonic
 * @param urlProvider
 * criar account no infura que iremos usar como gateway de acesso a rede ethereum
 * https://infura.io/
 */
const provider = new HDWalletProvider(
	mnemonic,
	urlProvider
);

const web3 = new Web3(provider);

/*
let accounts;


accounts = await web3.eth.getAccounts();
*/

// assynchronous call retorna uma Promise 
const deploy = async () => {

	const accounts = await web3.eth.getAccounts();

	console.log('Attempting to deploy from account %s', accounts[0]);

	const factory = await new web3.eth.Contract(JSON.parse(compiledFactory.interface)).
					//contrato lottery não tem argumentos a serem passados 
					//pelo método construtor, então preciso somente passar o objeto data, no deploy
					deploy({data: '0x' + compiledFactory.bytecode }).
					send({from: accounts[0], gas: '2000000' });

	console.log('Contract Deployed from address: %s', factory.options.address);

	
	
	const index = fs.createWriteStream('addressFactory/index.js');

	index.write(`export const addressFactory='${factory.options.address}'`);

	index.end();	
};

deploy();