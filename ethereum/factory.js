import web3 from './web3';
import { addressFactory } from './addressFactory'
import CampaignFactory from './build/CrowdfundingFactory.json';

const factory = new web3.eth.Contract(
    JSON.parse(CampaignFactory.interface),
    addressFactory
);

export default factory;