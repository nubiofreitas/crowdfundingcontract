// solium-disable linebreak-style
pragma solidity ^0.4.24;

/**
 *  contrato com conciliação
 */

 //criando uma factory de contratos 
contract CrowdfundingFactory{

    address[] public deployedCampaigns;
    
    function createCampaign(uint minimum) public {
        address newCampaign = new Crowdfunding(minimum, msg.sender);
        deployedCampaigns.push(newCampaign);
    }
    
    function getDeployedCampaigns() public view returns (address[]) {
        return deployedCampaigns;
    }
}
 
 
contract Crowdfunding {
    
    struct Request {
        string description;
        uint value;
        address recipient;
        bool complete;
        mapping(address => bool) aprovals;
        uint aprovalCount;
    }
    
    address public manager;
    uint public minimumContribution;
    mapping(address => bool) public aprovers;
    Request[] public request;
    uint public contributors;
    
    modifier isManager() {
        require(msg.sender == manager, "Sender of this transaction is not the manager");
        _;
    }
	
    constructor (uint minimum, address creator) public {
        manager = creator;
        minimumContribution = minimum;
        contributors = 0;
    }
    
    //função para receber as contribuições
    //e armazenar os contribuintes
    function contribute() public payable {
        require(msg.value >= minimumContribution, "Contribution value is less than the minimum");
        aprovers[msg.sender] = true;
        contributors++;
    }
    
    function createRequest(string description, uint value, address recipient) public isManager {
        Request memory newRequest = Request({
            description : description,
            value: value,
            recipient: recipient,
            complete: false,
            aprovalCount: 0
        });
        request.push(newRequest);
    }
    
    function aproveRequest(uint index) public {
        //muito importante o storage pois iremos manipular a copia da estrutura dentro do armazenamento
        //e persistir as alterações
        Request storage _request = request[index];
        //verifica se o endereço esta inserido no mapping
        require(aprovers[msg.sender], "Sender of this transaction is not contributor");
        //verifica se este endereço já votou para nessa request
        require(!_request.aprovals[msg.sender], "Sender of this transaction has already approved this request");
        //marca o voto desse endereço
        _request.aprovals[msg.sender] = true;
        _request.aprovalCount++;
    }
    
    function finalizeRequest(uint index) public isManager {
        Request storage _request = request[index];
        //verifica se este request foi aprovada por mais da metade dos contributors
        require(_request.aprovalCount > (contributors / 2), "Request not approved by the majority");
        //esse request tem que estar ativo
        require(!_request.complete, "Request already completed");
        //envia a quantia
        _request.recipient.transfer(_request.value);
        //marca como completada esse request
        _request.complete = true;
    }
    
    function getRequestCount() public view returns (uint256) {
        return request.length;
    }

    function getSummary()public view returns(uint, uint, uint, uint, address) {
        return(
                minimumContribution,
                address(this).balance,
                request.length,
                contributors,
                manager
        );
    }
}