import Web3 from 'web3';

//por utilizar next.js e sua caracteristica server-side
//não irá importar se o usuário usa metamask ou não.
//const web3 = new Web3(window.web3.currentProvider);

let web3;

//primeiro verificados se estamos no browser 
//e depois de o provider do metamask foi definido
if(typeof window !== 'undefined' && typeof window.web3 !== 'undefined'){
    web3 = new Web3(window.web3.currentProvider);
}else{
    //we are on the browser *OR* the user is not running metamask
    const provider = new Web3.providers.HttpProvider(
        'https://rinkeby.infura.io/v3/f96bcde67b9c403594af276fc7015c58'
    );
    web3 = new Web3(provider);
}

export default web3;