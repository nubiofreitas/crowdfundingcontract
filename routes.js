const routes = require('next-routes');


module.exports = routes()
.add('/', '/')
.add('/campaign/new', '/campaign/new')
.add('/campaign/:address', '/campaign/show')
.add('/campaign/:address/request','/campaign/request/show')
.add('/campaign/:address/request/new','/campaign/request/new');
                        