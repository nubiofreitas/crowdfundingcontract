import React, { Component } from 'react';
import { Form, Input, Message, Button } from 'semantic-ui-react';
import web3 from '../ethereum/web3';
import Campaign from '../ethereum/campaign';
import { Router } from '../routes';

class contributeForm extends Component {
    
    state = {
        amount: '',
        errorMessage: '',
        successMessage: '',
        loading: false
    }
  
    handleChange = (e, {name, value}) => this.setState({ [name]: value });

    onSubmit = async (event) => {
        event.preventDefault();
        const { amount } = this.state;
        const campaign = Campaign(this.props.address)
        const summary = await campaign.methods.getSummary().call();

        try{
            this.setState({ successMessage: '', loading: true, errorMessage: '' });
            const accounts = await web3.eth.getAccounts();
            await campaign.methods.contribute().send({ 
                from: accounts[0], 
                value: web3.utils.toWei(amount, 'ether') });
            this.setState({ successMessage: 'Done !',  loading: false, amount: '', errorMessage: '' });
            
            Router.replaceRoute(`/campaign/${this.props.address}`);
        }catch(err){
            this.setState({ errorMessage: err.message, loading: false });
        }
    }

    render(){
        const { amount } = this.state;

        return (
        <Form onSubmit={this.onSubmit} error={!this.state.errorMessage ? false : true} success={!this.state.successMessage ? false : true}>
            <Form.Field>
                <label>Contribuição Mínima: {web3.utils.fromWei(this.props.minimumContribution, 'ether')} ETH</label>
                <Input
                    autoComplete='off'
                    icon='ethereum'                                    
                    //actionPosition='left'
                    //label="wei" 
                    //labelPosition="right"
                    placeholder="Amount"
                    name="amount"
                    value={ amount }
                    onChange={this.handleChange}
                    />
            </Form.Field>
            <Button primary icon="add circle" loading={this.state.loading} disabled={!this.state.amount} content="Contribuir"/>
            <Message error header="Oops!" content={this.state.errorMessage}/>
            <Message success header='Success!'content={this.state.successMessage} />
        </Form>
        );
    };
}

export default contributeForm;