import React, { Component } from 'react';
import { Row, Table, Button } from 'semantic-ui-react';
import Campaign from '../ethereum/campaign';
import web3 from '../ethereum/web3';
class RequestRow extends Component {

    state= {

    };

    onAprove = async (index) => {
        if(typeof index !== 'undefined'){
            try{
                const campaign = Campaign(this.props.address);
                const accounts = await web3.eth.getAccounts();
                await campaign.methods.aproveRequest(index).send({ from: accounts[0] });
                //this.render();
                //
            }catch(err){
                console.log(err.message);
            }
        }
    }
    
    onFinalize = async (index) => {
        const campaign = Campaign(this.props.address);
        const accounts = await web3.eth.getAccounts();
        await campaign.methods.finalizeRequest(index).send({ from: accounts[0] });
    }

    render() {
        const { Cell, Row } = Table;
        const { request, id, aproversCount } = this.props;
        const readyToFinalize = request.aprovalCount > aproversCount / 2;


        return (
            <Row disabled={request.complete} positive={readyToFinalize}>
                <Cell>{id}</Cell>
                <Cell>{request.description}</Cell>
                <Cell>{web3.utils.fromWei(request.value, 'ether')}</Cell>
                <Cell>{request.recipient}</Cell>
                <Cell>{request.aprovalCount}/{aproversCount}</Cell>
                <Cell>
                    {request.complete ? null : (
                    <Button
                        content="Aprovar"
                        color="green"
                        basic
                        value={this.props.id}
                        onClick={event => this.onAprove(event.target.value)}                        
                    />
                    )}
                </Cell>
                <Cell>
                    {request.complete ? null : (
                    <Button
                        content="Finalizar"
                        basic
                        color="teal"
                        value={this.props.id}
                        onClick={(event) => this.onFinalize(event.target.value)}
                    />
                    )}
                </Cell>
            </Row>
        );
    }
}

export default RequestRow;