const assert = require('assert');
const ganache = require('ganache-cli');

const Web3 = require('web3');

const provider = ganache.provider();

//o construtor do Web3 podera mudar conforme o provider
const web3 = new Web3(provider);

//constante entre chaves, porque estamos esperando que venha do arquivo compile
//as propriedades interface e bytecode
//pega os dois objetos do arquivo compilado, interface(ABI) 
//e os bytecodes do contrato para deploy
const compiledFactory = require('../ethereum/build/CrowdfundingFactory.json');
const compiledCampaign = require('../ethereum/build/Crowdfunding.json');
 

let accounts;
let factory;
let campaignAddress;
let campaign;
const INITIAL_VALUE = web3.utils.toWei('1', 'ether');

beforeEach(async () =>{
    accounts = await web3.eth.getAccounts();
    
    //criando uma instância da fábrica de contratos
    factory = await new web3.eth.Contract(JSON.parse(compiledFactory.interface))
    .deploy({ data: compiledFactory.bytecode })
    .send({ from: accounts[0], gas : '3000000' });
    
    await factory.methods.createCampaign(INITIAL_VALUE).send({from: accounts[0], gas: '3000000'});

    [campaignAddress] = await factory.methods.getDeployedCampaigns().call();
    campaign = await new web3.eth.Contract(
        JSON.parse(compiledCampaign.interface),
        campaignAddress
    );

});

describe('CrowdFunding Contract Factory' ,() => {   
    it('Deploys a Factory and a CrowdFunding Campaign' , () => {
		//assert.ok para verificar se a propriedade address foi definida
        assert.ok(factory.options.address);
        console.log('Factory Address: %s', factory.options.address);
        assert.ok(campaign.options.address);
        console.log('Campaign Address: %s', campaign.options.address);        
	});   
});

describe('Campaign Contracts' , () => {
    it('contribute', async () => {
        await campaign.methods.contribute().send({from: accounts[0], value: web3.utils.toWei('1','ether')});

        const players = await factory.methods.getDeployedCampaigns().call();
        assert.ok(players[0]);

    });

    it('validate the account creator of the contract', async () => {
        const manager = await campaign.methods.manager().call();

        assert.equal(accounts[0], manager);
    });

    it('Contributors is Aprovers?' , async () => {
        await campaign.methods.contribute().send({from: accounts[1], value: web3.utils.toWei('1','ether')});
        
        const isContributor = await campaign.methods.aprovers(accounts[1]).call();
        assert(isContributor);
    });

    it('Require minimum contribution' , async () => {
        try{
            await campaign.methods.contribute().send({from: accounts[1], value: web3.utils.toWei('0.05','ether')});
            assert(false);
        }catch(err){
            assert.ok(err);
        }
    });
    it('Only manager can submit request' , async () => {
        try{
            await campaign.methods.contribute().send({from: accounts[1], value: web3.utils.toWei('1','ether')});
            await campaign.methods.createRequest('test mocha', web3.utils.toWei('1','ether'), accounts[1]).send({from: accounts[1], value: '1000000'});
            assert(false);
        }catch(err){
            assert.ok(err);
        }
    });
    it('Manager submit request' , async () => {
        try{
            await campaign.methods.contribute().send({from: accounts[1], value: web3.utils.toWei('1','ether')});    
            await campaign.methods.createRequest("test mocha", web3.utils.toWei('1','ether'), accounts[1]).send({from: accounts[0], gas: '1000000'});
            const request = await campaign.methods.request(0).call();
            assert.ok(request);
        }catch(err){
            assert.ok(err);
        }
    });
    it('Check information request' , async () => {
        //await campaign.methods.contribute().send({from: accounts[1], value: web3.utils.toWei('1','ether')});    
        await campaign.methods.createRequest('test mocha', web3.utils.toWei('1','ether'), accounts[1]).send({from: accounts[0], gas: '1000000'});
        const request = await campaign.methods.request(0).call();
        assert.equal('test mocha', request.description);
    });
    it('Only manager finalize request?' , async () => {
        try{
            await campaign.methods.contribute().send({from: accounts[1], value: web3.utils.toWei('1','ether')});    
            await campaign.methods.contribute().send({from: accounts[2], value: web3.utils.toWei('1','ether')});
            //balance of account 4
            console.log('Balance of account[3]: %s', web3.utils.fromWei(web3.eth.getBalance(accounts[3]),'ether'));
            //criando a requisição
            await campaign.methods.createRequest('test mocha', web3.utils.toWei('1','ether'), accounts[3]).send({from: accounts[0], gas: '1000000'});
            //aprovando a requisição
            await campaign.methods.aproveRequest(0).send({from: accounts[1], gas: '1000000'});    
            await campaign.methods.aproveRequest(0).send({from: accounts[2], gas: '1000000'});
            //finalizando a requisição conta diferente de manager
            await campaign.methods.finalizeRequest(0).send({from: accounts[1], gas: '1000000'});
            //teste feito para falhar caso não falhe, erro no teste
            assert(false);
        }catch(err){
            assert.ok(err);
        }
    });
    it('Manager finalize request less 51% approvers?' , async () => {
        try{
            await campaign.methods.contribute().send({from: accounts[1], value: web3.utils.toWei('1','ether')});    
            await campaign.methods.contribute().send({from: accounts[2], value: web3.utils.toWei('1','ether')});
            //criando a requisição
            await campaign.methods.createRequest('test mocha', web3.utils.toWei('1','ether'), accounts[3]).send({from: accounts[0], gas: '1000000'});
            //aprovando a requisição
            await campaign.methods.aproveRequest(0).send({from: accounts[1], gas: '1000000'});    
            //finalizando a requisição
            await campaign.methods.finalizeRequest(0).send({from: accounts[0], gas: '1000000'});
            assert(false);
        }catch(err){
            assert.ok(err);
        }
    }); 
    it('Manager finalize request!' , async () => {
        await campaign.methods.contribute().send({from: accounts[1], value: web3.utils.toWei('1','ether')});    
        await campaign.methods.contribute().send({from: accounts[2], value: web3.utils.toWei('1','ether')});
        //balance of account 4
        const old_balance = web3.utils.fromWei(await web3.eth.getBalance(accounts[3]),'ether');
        //criando a requisição
        await campaign.methods.createRequest('test mocha', web3.utils.toWei('1','ether'), accounts[3]).send({from: accounts[0], gas: '1000000'});
        //aprovando a requisição
        await campaign.methods.aproveRequest(0).send({from: accounts[1], gas: '1000000'});    
        await campaign.methods.aproveRequest(0).send({from: accounts[2], gas: '1000000'});
        //finalizando a requisição 
        await campaign.methods.finalizeRequest(0).send({from: accounts[0], gas: '1000000'});
        //teste feito para falhar caso não falhe, erro no teste
        const request = await campaign.methods.request(0).call();

        const new_balance = web3.utils.fromWei(await web3.eth.getBalance(accounts[3]),'ether');

        assert.equal(request.complete, true);        

        console.log('\told_Balance of account[3]: %s', old_balance);
        console.log('\tnew_Balance of account[3]: %s', new_balance);
    }); 
});